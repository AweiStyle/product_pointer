package com.aweistyle.awei.productpointer;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class homePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
    }

    public void add(View view) {

        Intent intent = new Intent();
        intent.setClass(homePage.this,addPage.class);
        startActivity(intent);

    }

    public void search(View view) {

        Intent intent = new Intent();
        intent.setClass(homePage.this,searchPage.class);
        startActivity(intent);

    }

    public void inOut(View view) {

        Intent intent = new Intent();
        intent.setClass(homePage.this,inOutPage.class);
        startActivity(intent);

    }

    public void stock(View view) {

        Intent intent = new Intent();
        intent.setClass(homePage.this,stockPage.class);
        startActivity(intent);

    }
}
